#ifndef PRINCESS_H
#define PRINCESS_H
#include "object.h"


class Princess : public Object
{
    public:
        Princess(GLdouble x, GLdouble y, GLdouble z, char* textpath, GLenum texttype, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular, int shininess);

    protected:

    private:
};

#endif // PRINCESS_H
