#include "light.h"

Light::Light(GLenum light, vector < GLfloat > position, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular)
{
    this->light = light;
    this->position = position;
    this->ambient = ambient;
    this->diffuse = diffuse;
    this->specular = specular;
    this->speed = 0;
}

void Light::spawn(){
    glLightfv(this->light, GL_POSITION, this->position.data());
    glLightfv(this->light, GL_AMBIENT, this->ambient.data());
    glLightfv(this->light, GL_DIFFUSE, this->diffuse.data());
    glLightfv(this->light, GL_SPECULAR, this->specular.data());
}

void Light::setSpeed(GLfloat speed){
    this->speed = speed;
}

void Light::moveUp(){
    this->position.data()[1] += this->speed;
}

void Light::moveDown(){
    this->position.data()[1] -= this->speed;
}
