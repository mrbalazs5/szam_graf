#include "player.h"

Object Player::torso(0, 0, 0, "./textures/croc.jpg",GL_RGB, {0.2, 0.2, 0.2, 1}, {0.8, 0.8, 0.8, 1}, {1, 1, 1, 1}, 60);

Object Player::left_arm(0, 0, 0, "./textures/croc.jpg",GL_RGB, {0.2, 0.2, 0.2, 1}, {0.8, 0.8, 0.8, 1}, {1, 1, 1, 1}, 60);
Object Player::right_arm(0, 0, 0, "./textures/croc.jpg",GL_RGB, {0.2, 0.2, 0.2, 1}, {0.8, 0.8, 0.8, 1}, {1, 1, 1, 1}, 60);

Object Player::left_leg(0, 0, 0, "./textures/croc.jpg",GL_RGB, {0.2, 0.2, 0.2, 1}, {0.8, 0.8, 0.8, 1}, {1, 1, 1, 1}, 60);
Object Player::right_leg(0, 0, 0, "./textures/croc.jpg",GL_RGB, {0.2, 0.2, 0.2, 1}, {0.8, 0.8, 0.8, 1}, {1, 1, 1, 1}, 60);

Object Player::right_eye(0.13, 0.53, 0.35, "./textures/croc_eye.jpg",GL_RGB, {0.2, 0.2, 0.2, 1}, {0.8, 0.8, 0.8, 1}, {1, 1, 1, 1}, 60);
Object Player::left_eye(-0.13, 0.53, 0.35, "./textures/croc_eye.jpg",GL_RGB, {0.2, 0.2, 0.2, 1}, {0.8, 0.8, 0.8, 1}, {1, 1, 1, 1}, 60);

Player::Player(GLdouble x, GLdouble y, GLdouble z)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->rl_angle = 0;
    this->rl_forward = true;
    this->moveLegs = false;
    this->angle = 180;
    this->jump = true;
    this->fly = false;
}

GLdouble Player::getX(){
    return this->x;
}

GLdouble Player::getY(){
    return this->y;
}

GLdouble Player::getZ(){
    return this->z;
}

bool Player::getFly(){
    return this->fly;
}

void Player::setX(GLdouble x){
    this->x = x;
}

void Player::setY(GLdouble y){
    this->y = y;
}

void Player::setZ(GLdouble z){
    this->z = z;
}

GLdouble Player::getAngle(){
    return this->angle;
}

void Player::setMoveLegs(bool moveLegs){
    this->moveLegs = moveLegs;
}

void Player::setJumping(bool jump){
    this->jump = jump;
}

void Player::setFly(bool fly){
    this->fly = fly;
}

void Player::setPos(GLdouble x, GLdouble y, GLdouble z){
    this->x = x;
    this->y = y;
    this->z = z;
}

void Player::render(){

    glPushMatrix();

    glTranslated(this->x, this->y, this->z);
    glRotated(this->angle, 0, 1, 0);


    torso.render();

    glPushMatrix();
        glTranslated(0.4, 0.15, -0.3);
        if(this->moveLegs){
            glRotated(this->rl_angle, 1, 0, 0);
        }
        left_arm.render();
    glPopMatrix();

    glPushMatrix();
    glTranslated(-0.4, 0.15, -0.3);
        if(this->moveLegs){
            glRotated(-this->rl_angle, 1, 0, 0);
        }
        right_arm.render();
    glPopMatrix();

    glPushMatrix();
        glTranslated(-0.35, -0.6, -0.35);
        if(this->moveLegs){
                glRotated(this->rl_angle, 1, 0, 0);
        }
        right_leg.render();
    glPopMatrix();

    glPushMatrix();
        glTranslated(0.35, -0.6, -0.35);
        if(this->moveLegs){
            glRotated(-this->rl_angle, 1, 0, 0);
        }
        left_leg.render();
    glPopMatrix();

    left_eye.render();
    right_eye.render();

    glPopMatrix();

    if(this->rl_angle <= -55){
        this->rl_forward = false;
    }
    if(this->rl_angle >= 55){
        this->rl_forward = true;
    }

    if(this->rl_forward){
        this->rl_angle -= 0.3;
    }else{
        this->rl_angle += 0.3;
    }

}

void Player::init(){
    torso.loadObj("./models/croc_torso.obj");
    left_arm.loadObj("./models/croc_left_arm.obj");
    right_arm.loadObj("./models/croc_right_arm.obj");
    right_leg.loadObj("./models/croc_right_leg.obj");
    left_leg.loadObj("./models/croc_left_leg.obj");
    left_eye.loadObj("./models/croc_left_eye.obj");
    right_eye.loadObj("./models/croc_right_eye.obj");

}

void Player::setSpeed(GLdouble speed){
    this->speed = speed;
}

void Player::moveForward(){
    this->z -= this->speed;
}

void Player::moveBack(){
    this->z += this->speed;
}

void Player::moveLeft(){
    this->x -= this->speed;
}

void Player::moveRight(){
    this->x += this->speed;
}

void Player::moveUp(){
    if(this->jump){
        this->setMoveLegs(true);
        this->setSpeed(0.1);
        this->y += this->speed;
    }

    if(this->y >= 4 || this->y <= -0.5){
        this->jump = false;
    }
}

void Player::turnLeft(){
    this->angle -= 1.8;
}

void Player::turnRight(){
    this->angle += 1.8;
}

