#ifdef __APPLE__
#else
#endif

#include <stdlib.h>
#include "display.h"

int main(int argc, char** argv){

    Display display("Croco's Adventure Through The River", 0,0, 640, 480, argc, argv);
    display.run();

    return EXIT_SUCCESS;
}

