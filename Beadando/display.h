#ifndef DISPLAY_H
#define DISPLAY_H
#include "object.h"
#include "camera.h"
#include <time.h>
#include<iostream>
#include "player.h"
#include "light.h"
#include "box.h"
#include "water.h"
#include "land.h"
#include "house.h"
#include "sky.h"
#include "boat.h"
#include "coin.h"
#include "collisionmanager.h"
#include <map>
#include <string>
#include <cstring>
using std::string;
using std::map;

/*
    This class is responsible for the display, render scene, register callbacks, handle events
*/

class Display
{
    public:
        //ctor
        Display(char* winTitle, int winPosX, int winPosY, int winWidth, int winHeight, int argc, char** argv);
        //Run the program, window creation, initialization, set global OpenGL attributes
        void run();
        //variables for calculating FPS


    protected:

    private:

        static int initial_time, final_time, frame_count;
        //initialize window attributes
        void init(int argc, char** argv);
        //initialize OpenGL attributes
        void setOpenGLAttributes();
        //drawing models, handle changes
        static void renderScene();
        //handle mouse button events
        static void mouseButton(int button, int state, int x, int y);
        //handle window reshape event
        static void reshape(int w, int h);
        //register callbacks
        void registerCallbacks();
        //handle normal keyboard events
        static void pressKeys(unsigned char key, int x, int y);
        //handle button releases
        static void releaseKeys(unsigned char key, int x, int y);
        //handle passive mouse motion events
        static void mouseMove(int x, int y);

        static void pressSpecialKeys(int key, int x, int y);

        static void handleKeys();

        static void displayText( GLfloat x, GLfloat y, GLfloat z, GLint r, GLint g, GLint b, const char *string );

        char* winTitle;
        int winPosX, winPosY, winWidth, winHeight;

        static bool showMenu;

        static Camera camera;

        static Player player;

        static Light light;

        static Sky skybox1;
        static Sky skybox2;
        static Sky skybox3;
        static Sky skybox4;
        static Sky skybox5;
        static Sky skybox6;

        static Land land1;
        static Land land2;

        static Water water;

        static Box box;
        static Box box1;
        static Box box2;
        static Box box3;
        static Box box4;
        static Box box5;
        static Box box6;
        static Box box7;
        static Box box8;
        static Box box9;
        static Box box10;
        static Box box11;
        static Box box12;
        static Box box13;
        static Box box14;
        static Box box15;
        static Box box16;

        static House house;

        static House house2;

        static Boat boat;

        static Player princess;

        static Coin coin1;

        static GLdouble coinAngle;

        static bool* keyStates;

        static CollisionManager cmanager;

        static map < string, Box > boxes;
        static map < string, Land > lands;
        static map < string, Coin > coins;

};

#endif // DISPLAY_H
