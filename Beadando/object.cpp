#include "object.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
using std::vector;

Object::Object(GLdouble x, GLdouble y, GLdouble z, char* textpath, GLenum texttype, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular, int shininess)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->textpath = textpath;
    this->texttype = texttype;
    this->ambient = ambient;
    this->diffuse = diffuse;
    this->specular = specular;
    this->shininess = shininess;
}

GLdouble Object::getX(){
    return this->x;
}

GLdouble Object::getY(){
    return this->y;
}

GLdouble Object::getZ(){
    return this->z;
}

void Object::setX(GLdouble x){
    this->x = x;
}

void Object::setY(GLdouble y){
    this->y = y;
}

void Object::setZ(GLdouble z){
    this->z = z;
}

void Object::loadTexture(){

    glGenTextures(1, &this->texture);

    this->data = stbi_load(this->textpath, &this->width, &this->height, &this->nrChannels, 0);

    glBindTexture(GL_TEXTURE_2D, this->texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);

    gluBuild2DMipmaps( GL_TEXTURE_2D, 4, this->width, this->height, this->texttype, GL_UNSIGNED_BYTE, this->data );


    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    stbi_image_free(this->data);

}

void Object::loadObj(char *fname){
    FILE *fp;
    GLfloat x, y, z;
    char lineHeader[128];
    this->dlist = glGenLists(1);

    vector < vector < GLfloat > > vertices;
    vector < GLfloat > vertex;

    vector < vector < GLfloat > > input_normals;
    vector < GLfloat > input_normal;
    vector < vector < GLfloat > > normals;
    vector < GLfloat > normal;

    vector < vector < GLfloat > > input_uvs;
    vector < GLfloat > input_uv;
    vector < vector < GLfloat > > uvs;
    vector < GLfloat > uv;

    vector < vector < int> > faces;
    vector < int > face;

    unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];

    fp = fopen(fname, "r");

    if(!fp){
        printf("can't open file %s\n", fname);
        exit(1);
    }

    while(1){
        int res = fscanf(fp, "%s", lineHeader);
        if(res == EOF){
            break;
        }

        if(strcmp(lineHeader, "v") == 0){
            fscanf(fp, "%f %f %f", &x, &y, &z);
            vertex.push_back(x);
            vertex.push_back(y);
            vertex.push_back(z);
            vertices.push_back(vertex);
            vertex.clear();
        }else if(strcmp(lineHeader, "vn") == 0){
            fscanf(fp, "%f %f %f", &x, &y, &z);
            input_normal.push_back(x);
            input_normal.push_back(y);
            input_normal.push_back(z);
            input_normals.push_back(input_normal);
            input_normal.clear();
        }else if(strcmp(lineHeader, "vt") == 0){
             fscanf(fp, "%f %f", &x, &y);
             input_uv.push_back(x);
             input_uv.push_back(y);
             input_uvs.push_back(input_uv);
             input_uv.clear();
        }else if(strcmp(lineHeader, "f") == 0){
                 fscanf(fp, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);

                 face.push_back(vertexIndex[0]);
                 face.push_back(vertexIndex[1]);
                 face.push_back(vertexIndex[2]);
                 faces.push_back(face);
                 face.clear();

                 uv.push_back(uvIndex[0]);
                 uv.push_back(uvIndex[1]);
                 uv.push_back(uvIndex[2]);
                 uvs.push_back(uv);
                 uv.clear();

                 normal.push_back(normalIndex[0]);
                 normal.push_back(normalIndex[1]);
                 normal.push_back(normalIndex[2]);
                 normals.push_back(normal);
                 normal.clear();

        }
    }

    fclose(fp);

    this->loadTexture();

    GLfloat ambient[] = {0.8, 0.8, 0.8, 1};
    GLfloat diffuse[] = {0.2, 0.2, 0.2, 1};
    GLfloat specular[] = {1.0, 1.0, 1.0, 1.0};

    glNewList(this->dlist, GL_COMPILE);

    glPushMatrix();

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, this->ambient.data());
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, this->diffuse.data());
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, this->specular.data());
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, this->shininess);
    glBindTexture(GL_TEXTURE_2D, this->texture);

    glTranslated(this->x, this->y, this->z);

            glBegin(GL_TRIANGLES);
                for(int i = 0; i < faces.size(); i++){

                    for(int j = 0; j < faces[i].size(); j++){
                        glTexCoord2f(input_uvs[uvs[i][j] - 1].data()[0], 1.0f - input_uvs[uvs[i][j] - 1].data()[1]);
                        glNormal3fv(input_normals[normals[i][j] - 1].data());
                        glVertex3fv(vertices[faces[i][j] - 1].data());
                    }
                }
            glEnd();

    glPopMatrix();

    glEndList();

}

void Object::render(){

    glPushMatrix();

    glCallList(this->dlist);

    glPopMatrix();

}








