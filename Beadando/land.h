#ifndef LAND_H
#define LAND_H
#include "object.h"

class Land : public Object
{
    public:
        Land(GLdouble x, GLdouble y, GLdouble z, char* textpath, GLenum texttype, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular, int shininess);

    protected:

    private:
};

#endif // LAND_H
