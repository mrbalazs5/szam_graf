#ifndef BOX_H
#define BOX_H
#include "object.h"

class Box : public Object
{
    public:
        Box(GLdouble x, GLdouble y, GLdouble z, char* textpath, GLenum texttype, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular, int shininess);

    protected:

    private:
};

#endif // BOX_H
