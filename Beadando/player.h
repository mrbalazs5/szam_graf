#ifndef PLAYER_H
#define PLAYER_H
#include "object.h"
#include "math.h"

/*
    This class represents the player
*/

class Player
{
    public:
        //ctor
        Player(GLdouble x, GLdouble y, GLdouble z);
        //draw the player(torso, limbs etc.)
        void render();
        //initialize the player
        void init();
                //getter methods
        GLdouble getX();
        GLdouble getY();
        GLdouble getZ();
        GLdouble getAngle();
        bool getFly();
        //setter methods
        void setX(GLdouble x);
        void setY(GLdouble y);
        void setZ(GLdouble z);
        void setSpeed(GLdouble speed);
        void setJumping(bool jump);
        void setPos(GLdouble x, GLdouble y, GLdouble z);
        void setFly(bool fly);
        //Moves the player forward
        void moveForward();
        //Moves the player backward
        void moveBack();
        //Move the player left
        void moveLeft();
        //Move the player right
        void moveRight();

        void moveUp();

        void turnLeft();

        void turnRight();

        void setMoveLegs(bool moveLegs);
    protected:

    private:
        GLdouble x, y, z, rl_angle, speed, angle;
        static Object torso;
        static Object left_arm;
        static Object right_arm;
        static Object right_leg;
        static Object left_leg;
        static Object right_eye;
        static Object left_eye;
        bool rl_forward;
        bool moveLegs;
        bool jump;
        bool fly;
};

#endif // PLAYER_H
