#ifndef CAMERA_H
#define CAMERA_H
#include <GL/glut.h>
#include "math.h"
#include "player.h"

/*
    This class handles the camera.
*/


class Camera
{
    public:
        //ctor
        Camera(GLdouble x, GLdouble y, GLdouble z, GLdouble lx, GLdouble ly, GLdouble lz, GLdouble distance);

        GLdouble getLx();

        GLdouble getLy();

        GLdouble getLz();

        //"Renders" the camera, so we can change its position, view
        void render();
        //Moves the camera forward
        void moveForward();
        //Moves the camera backward
        void moveBack();
        //Move the camera up
        void moveUp();
        //Move the camera down
        void moveDown();
        //Move the camera left
        void moveLeft();
        //Move the camera right
        void moveRight();
        //Look left
        void lookLeft();
        //Look right
        void lookRight();
        //Stops the camera
        void stop();
        //Sets the speed attribute
        void setSpeed(GLdouble speed);

        void follow(Player player);

    protected:

    private:
        //       position     look
        GLdouble x, y, z, lx, ly, lz, speed, angle, deltaAngle, distance;
};

#endif // CAMERA_H
