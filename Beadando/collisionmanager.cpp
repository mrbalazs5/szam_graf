#include "collisionmanager.h"

CollisionManager::CollisionManager(Water &water, House &house, Player &player, Player &princess) : player(player ), house (house), water(water), princess(princess)
{
    this->water = water;
    this->house = house;
    this->player = player;
    this->princess = princess;
}

void CollisionManager::init(map < string, Box > boxes, map < string, Land > lands, map < string, Coin > coins, Water &water, House &house, Player &player, Player &princess){
    this->boxes = boxes;
    this->lands = lands;
    this->coins = coins;
    this->water = water;
    this->house = house;
    this->player = player;
    this->princess = princess;
    this->gravity = 0.02;
}

void CollisionManager::checkBoxes(){
    for(auto box : boxes){

        this->checkCollision(box.second, 2.3, 1);

    }
}
template <class Object>
bool CollisionManager::checkCollision(Object object, GLdouble x, GLdouble y){
    bool collide = false;
   if(
           (this->player.getY() <= object.getY() + x) &&
           (this->player.getY() >= object.getY() + (x - 0.1)) &&
           (this->player.getX() - 1 <= object.getX() + y) &&
           (this->player.getX() + 1 >= object.getX() - y) &&
           (this->player.getZ() - 1 <= object.getZ() + y) &&
           (this->player.getZ() + 1 >= object.getZ() - y)
          ){
            collide = true;
            this->player.setY(object.getY() + x);
            this->player.setJumping(true);
        }else if(
           (this->player.getY() >= object.getY() - x) &&
           (this->player.getY() <= object.getY() - (x - 0.1)) &&
           (this->player.getX() <= object.getX() + y) &&
           (this->player.getX() >= object.getX() - y) &&
           (this->player.getZ() <= object.getZ() + y) &&
           (this->player.getZ() >= object.getZ() - y)
          ){
            collide = true;
            this->player.setY(object.getY() - x);
            this->player.setJumping(true);
        }else if(
           (this->player.getX() <= object.getX() + x) &&
           (this->player.getX() >= object.getX() + (x - 0.1)) &&
           (this->player.getY() - 1 <= object.getY() + y) &&
           (this->player.getY() + 1 >= object.getY() - y) &&
           (this->player.getZ() - 1 <= object.getZ() + y) &&
           (this->player.getZ() + 1 >= object.getZ() - y)
          ){
            collide = true;
            this->player.setX(object.getX() + x);
        }else if(
           (this->player.getX() >= object.getX() - x) &&
           (this->player.getX() <= object.getX() - (x - 0.1)) &&
           (this->player.getY() - 1 <= object.getY() + y) &&
           (this->player.getY() + 1 >= object.getY() - y) &&
           (this->player.getZ() - 1 <= object.getZ() + y) &&
           (this->player.getZ() + 1 >= object.getZ() - y)
          ){
            collide = true;
            this->player.setX(object.getX() - x);
        }else if(
           (this->player.getZ() <= object.getZ() + (x - 0.3)) &&
           (this->player.getZ() >= object.getZ() + (x - 0.4)) &&
           (this->player.getY() - 1 <= object.getY() + y) &&
           (this->player.getY() + 1 >= object.getY() - y) &&
           (this->player.getX() - 1 <= object.getX() + y) &&
           (this->player.getX() + 1 >= object.getX() - y)
          ){
            collide = true;
            this->player.setZ(object.getZ() + (x - 0.3));
        }else if(
           (this->player.getZ() >= object.getZ() - (x - 0.3)) &&
           (this->player.getZ() <= object.getZ() - (x - 0.4)) &&
           (this->player.getY() - 1 <= object.getY() + y) &&
           (this->player.getY() + 1 >= object.getY() - y) &&
           (this->player.getX() - 1 <= object.getX() + y) &&
           (this->player.getX() + 1 >= object.getX() - y)
          ){
            collide = true;
            this->player.setZ(object.getZ() - (x - 0.3));
           }

    return collide;
}

void CollisionManager::checkLands(){
    for(auto land : lands){

    this->checkCollision(land.second, 2.3, 100);

    }
}

void CollisionManager::checkCoins(){
    for(auto coin : coins){
        if(this->checkCollision(coin.second, 1.5, 1)){
            exit(0);
        }

    }
}

void CollisionManager::watch(){
    if(!player.getFly()){
        this->player.setY(player.getY() - this->gravity);
    }

    this->checkLands();

    this->checkBoxes();

    this->checkCoins();

}
