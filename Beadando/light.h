#ifndef LIGHT_H
#define LIGHT_H
#include <GL/glut.h>
#include <vector>
using std::vector;

/*
    This class represents the lights.
*/

class Light
{
    public:
        //ctor
        Light(GLenum light, vector < GLfloat > position, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular);
        //set the speed attribute of the light(for moving the light up and down)
        void setSpeed(GLfloat speed);
        //move the light upward
        void moveUp();
        //move the light downward
        void moveDown();
        //initialize the light
        void spawn();
    protected:

    private:
        GLenum light;
        GLfloat speed;
        vector < GLfloat > position, ambient, diffuse, specular;
};

#endif // LIGHT_H
