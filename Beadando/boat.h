#ifndef BOAT_H
#define BOAT_H
#include "object.h"

class Boat : public Object
{
    public:
        Boat(GLdouble x, GLdouble y, GLdouble z, char* textpath, GLenum texttype, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular, int shininess);

    protected:

    private:
};

#endif // BOAT_H
