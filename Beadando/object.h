#ifndef OBJECT_H
#define OBJECT_H
#include <GL/glut.h>
#include <iostream>
#include <stdio.h>
#include <vector>
using std::vector;

/*
    This class represents the models.
*/

class Object
{
    public:
        //ctor
        Object(GLdouble x, GLdouble y, GLdouble z, char* textpath, GLenum texttype, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular, int shininess);
        //draw the models to the screen
        void render();
        //load the objects form an OBJ file and add textures to them
        void loadObj(char *fname);
        //getter methods
        GLdouble getX();
        GLdouble getY();
        GLdouble getZ();
        //setter methods
        void setX(GLdouble x);
        void setY(GLdouble y);
        void setZ(GLdouble z);

    protected:
        void loadTexture();
        GLdouble x, y, z;
        //display list for drawing
        GLuint dlist;
        //store the texture of the object
        GLuint texture;
        //path of the texture image
        char* textpath;
        GLenum texttype;
        unsigned char* data;
        GLint width, height, nrChannels;

        vector < GLfloat > ambient, diffuse, specular;
        int shininess;

    private:


};

#endif // OBJECT_H
