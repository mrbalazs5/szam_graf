#ifndef HOUSE_H
#define HOUSE_H
#include "object.h"


class House : public Object
{
    public:
        House(GLdouble x, GLdouble y, GLdouble z, char* textpath, GLenum texttype, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular, int shininess);

    protected:

    private:
};

#endif // HOUSE_H
