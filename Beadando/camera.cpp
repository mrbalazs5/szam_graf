#include "camera.h"

Camera::Camera(GLdouble x, GLdouble y, GLdouble z, GLdouble lx, GLdouble ly, GLdouble lz, GLdouble distance)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->lx = lx;
    this->ly = ly;
    this->lz = lz;
    this->speed = 0;
    this->angle = 0;
    this->deltaAngle = 0;
    this->deltaAngle = 0;
    this->distance = distance;
}

GLdouble Camera::getLx(){
    return this->lx;
}

GLdouble Camera::getLy(){
    return this->ly;
}

GLdouble Camera::getLz(){
    return this->lz;
}

void Camera::render(){
    this->angle -= deltaAngle;
    this->x = this->lx + this->distance * sin(angle);
    this->y = this->ly + 1;
    this->z = this->lz + this->distance * cos(angle);

    gluLookAt(this->x, this->y, this->z,
              this->lx, this->ly, this->lz,
              0, 1, 0);
}

void Camera::follow(Player player){
    this->lx = player.getX();
    this->ly = player.getY();
    this->lz = player.getZ();
}

void Camera::setSpeed(GLdouble speed){
    this->speed = speed;
}

void Camera::moveForward(){
    this->z -= this->speed;
}

void Camera::moveBack(){
    this->z += this->speed;
}

void Camera::moveUp(){
    this->y += this->speed;
}

void Camera::moveDown(){
    this->y -= this->speed;
}

void Camera::moveLeft(){
    this->x -= this->speed;
}

void Camera::moveRight(){
    this->x += this->speed;
}

void Camera::lookLeft(){
    this->deltaAngle = 0.01;

}

void Camera::lookRight(){
    this->deltaAngle = -0.01;
}

void Camera::stop(){
    this->deltaAngle = 0;
}


