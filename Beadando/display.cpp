#include "display.h"

int Display::initial_time = time(NULL);
int Display::frame_count = 0;
int Display::final_time;
bool Display::showMenu = false;

Player Display::player(0, 0, 0);

Player Display::princess(0, 0, 105);

Light Display::light(GL_LIGHT0, {0, 0, 0, 1.0}, {0.2, 0.2, 0.2, 1.0}, {0.8, 0.8, 0.8, 1.0}, {1.0, 1.0, 1.0, 1.0});
Camera Display::camera(0, 0, 0, player.getX(), player.getY(), player.getZ(), 5);

Sky Display::skybox1(0, 0, 0, "./textures/skybox2.jpg", GL_RGB, {1, 1, 1, 1}, {0, 0, 0, 1}, {1, 1, 1, 1}, 60);
Sky Display::skybox2(0, 0, 0, "./textures/skybox2.jpg", GL_RGB,{1, 1, 1, 1}, {0, 0, 0, 1}, {1, 1, 1, 1}, 60);
Sky Display::skybox3(0, 0, 0, "./textures/skybox2.jpg", GL_RGB,{1, 1, 1, 1}, {0, 0, 0, 1}, {1, 1, 1, 1}, 60);
Sky Display::skybox4(0, 0, 0, "./textures/skybox2.jpg", GL_RGB,{1, 1, 1, 1}, {0, 0, 0, 1}, {1, 1, 1, 1}, 60);
Sky Display::skybox5(0, 0, 0, "./textures/skybox2.jpg", GL_RGB,{1, 1, 1, 1}, {0, 0, 0, 1}, {1, 1, 1, 1}, 60);
Sky Display::skybox6(0, 0, 0, "./textures/skybox2.jpg", GL_RGB,{1, 1, 1, 1}, {0, 0, 0, 1}, {1, 1, 1, 1}, 60);


Land Display::land1(0, -2.3, 90, "./textures/dirt.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Land Display::land2(0, -2.3, -200, "./textures/dirt.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);

Water Display::water(0, -2, -110, "./textures/water.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);

Box Display::box(0, -1.5, -12, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box1(0, -1.5, -18, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box2(0, -1.5, -24, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box3(2, -1.5, -30, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box4(0, -1.5, -36, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box5(-2, -1.5, -42, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box6(3, -1.5, -48, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box7(0, -1.5, -54, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box8(0, -1.5, -60, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box9(0, -1.5, -66, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box10(0, -1.5, -72, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box11(0, -1.5, -78, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box12(0, -1.5, -84, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box13(0, -1.5, -90, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box14(0, -1.5, -96, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box15(3, -0.5, 0, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);
Box Display::box16(-5.5, -1.5, -76, "./textures/box.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);

House Display::house(0, -1.5, -120, "./textures/house.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);

House Display::house2(0, -1.5, -25, "./textures/house2.png", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);

Boat Display::boat(15, -2.5, -25, "./textures/boat.jpg", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);

Coin Display::coin1(-5.5, 0.5, -76, "./textures/coin.png", GL_RGB,{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, 60);

GLdouble Display::coinAngle = 0;

bool* Display::keyStates = new bool[256];

map < string, Box > Display::boxes;
map < string, Land > Display::lands;
map < string, Coin > Display::coins;
CollisionManager Display::cmanager(water, house, player, princess);

Display::Display(char* winTitle, int winPosX, int winPosY, int winWidth, int winHeight, int argc, char** argv)
{
    //init window attributes
    this->winTitle = winTitle;
    this->winPosX = winPosX;
    this->winPosY = winPosY;
    this->winWidth = winWidth;
    this->winHeight = winHeight;
    //init window
    this->init(argc, argv);

    for(int i = 0; i < 256; i++){
        keyStates[i] = false;
    }
}


void Display::init(int argc, char** argv){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
    glutInitWindowSize(this->winWidth, this->winHeight);
    glutInitWindowPosition(this->winPosX, this->winPosY);

    lands.insert(std::pair<string, Land>("land1", land1));
    lands.insert(std::pair<string, Land>("land2", land2));

    boxes.insert(std::pair<string, Box>("box", box));
    boxes.insert(std::pair<string, Box>("box1", box1));
    boxes.insert(std::pair<string, Box>("box2", box2));
    boxes.insert(std::pair<string, Box>("box3", box3));
    boxes.insert(std::pair<string, Box>("box4", box4));
    boxes.insert(std::pair<string, Box>("box5", box5));
    boxes.insert(std::pair<string, Box>("box6", box6));
    boxes.insert(std::pair<string, Box>("box7", box7));
    boxes.insert(std::pair<string, Box>("box8", box8));
    boxes.insert(std::pair<string, Box>("box9", box9));
    boxes.insert(std::pair<string, Box>("box10", box10));
    boxes.insert(std::pair<string, Box>("box11", box11));
    boxes.insert(std::pair<string, Box>("box12", box12));
    boxes.insert(std::pair<string, Box>("box13", box13));
    boxes.insert(std::pair<string, Box>("box14", box14));
    boxes.insert(std::pair<string, Box>("box15", box15));
    boxes.insert(std::pair<string, Box>("box16", box16));

    coins.insert(std::pair<string, Coin>("coin1", coin1));



    cmanager.init(boxes, lands, coins, water, house, player, princess);
}


void Display::setOpenGLAttributes(){
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    glutSetCursor(GLUT_CURSOR_NONE);

    glEnable(GL_LIGHT0);
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
    glEnable(GL_LIGHTING);

}

void Display::displayText( GLfloat x, GLfloat y, GLfloat z, GLint r, GLint g, GLint b, const char *string ) {
	int j = strlen( string );
    glDisable(GL_LIGHTING);
    glPushMatrix();

    glEnable(GL_COLOR_MATERIAL);
	glColor3f( r, g, b );
	glRasterPos3d(x, y, z);
	for( int i = 0; i < j; i++ ) {
		glutBitmapCharacter( GLUT_BITMAP_TIMES_ROMAN_24, string[i] );
	}
	 glDisable(GL_COLOR_MATERIAL);

	glPopMatrix();
	glEnable(GL_LIGHTING);
}

void Display::handleKeys(){
    if(keyStates['q']){
       player.setPos(0, 0, 0);
    }

    if(keyStates['w']){
       player.setSpeed(0.02);
       player.setMoveLegs(true);
       player.moveForward();
    }

    if(!keyStates['w']){
        player.setMoveLegs(false);
        player.setSpeed(0);
    }

    if(keyStates['s']){
       player.setSpeed(0.02);
       player.setMoveLegs(true);
       player.moveBack();
    }

    if(!keyStates['s']){
       player.setSpeed(0);
    }

    if(keyStates['a']){
       player.setSpeed(0.02);
       player.setMoveLegs(true);
       player.moveLeft();
    }

    if(!keyStates['a']){
       player.setSpeed(0);
    }

    if(keyStates['d']){
       player.setSpeed(0.02);
       player.setMoveLegs(true);
       player.moveRight();
    }

    if(!keyStates['d']){
       player.setSpeed(0);
    }

    if(keyStates['+']){
      light.setSpeed(0.1);
      light.moveUp();
    }

    if(!keyStates['+']){
       light.setSpeed(0);
    }

    if(keyStates['-']){
      light.setSpeed(0.1);
      light.moveDown();
    }

    if(!keyStates['-']){
       light.setSpeed(0);
    }

    if(keyStates[32]){
       player.moveUp();
    }

    if(keyStates['e']){
       player.setFly(true);
    }

    if(!keyStates['e']){
       player.setFly(false);
    }

    if(!keyStates[32]){
       player.setSpeed(0);
    }

     if(keyStates[27]){
      exit(0);
    }
}

void Display::renderScene(){

    handleKeys();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();

    cmanager.watch();

    light.spawn();

    glutWarpPointer((glutGet(GLUT_WINDOW_WIDTH) / 2), (glutGet(GLUT_WINDOW_HEIGHT) / 2));

    camera.follow(player);
    camera.render();

    if(showMenu){
        displayText(camera.getLx(), camera.getLy() + 2, camera.getLz(), 255, 0, 0, "   CONTROLLS");
        displayText(camera.getLx(), camera.getLy() + 1.8, camera.getLz(), 255, 255, 255, "ESC - EXIT");
        displayText(camera.getLx(), camera.getLy() + 1.65, camera.getLz(), 255, 255, 255, "W - GO FORWARD");
        displayText(camera.getLx(), camera.getLy() + 1.50, camera.getLz(), 255, 255, 255, "A - GO LEFT");
        displayText(camera.getLx(), camera.getLy() + 1.35, camera.getLz(), 255, 255, 255, "S - GO RIGHT");
        displayText(camera.getLx(), camera.getLy() + 1.20, camera.getLz(), 255, 255, 255, "D - GO BACKWARD");
        displayText(camera.getLx(), camera.getLy() + 1.05, camera.getLz(), 255, 255, 255, "+ - MOVE THE LIGHT UP");
        displayText(camera.getLx(), camera.getLy() + 0.9, camera.getLz(), 255, 255, 255, "- - MOVE THE LIGHT DOWN");
        displayText(camera.getLx(), camera.getLy() + 0.75, camera.getLz(), 255, 255, 255, "Q - RESET POSITION");
    }

    skybox1.render();
    skybox2.render();
    skybox3.render();
    skybox4.render();
    skybox5.render();
    skybox6.render();

    land1.render();
    land2.render();

    water.render();

    box.render();
    box1.render();
    box2.render();
    box3.render();
    box4.render();
    box5.render();
    box6.render();
    box7.render();
    box8.render();
    box9.render();
    box10.render();
    box11.render();
    box12.render();
    box13.render();
    box14.render();
    box15.render();
    box16.render();

    player.render();

    glPushMatrix();
        glRotated(180, 0, 1, 0);
        princess.render();
    glPopMatrix();

    house.render();

    glPushMatrix();
        glRotated(170, 0, 1, 0);
        house2.render();
    glPopMatrix();

    boat.render();

    coin1.render();


    glutPostRedisplay();

    glutSwapBuffers();

    coinAngle += 0.1;

    frame_count++;
    final_time = time(NULL);

    if((final_time - initial_time) > 0){
        std::cout << "FPS: " << frame_count / (final_time - initial_time) << std::endl;
        frame_count = 0;
        initial_time = final_time;
    }

}

void Display::reshape(int w, int h){
    if(h == 0){
        h = 1;
    }

    float ratio = 1.0 * w / h;

    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    glViewport(0, 0, w, h);
    gluPerspective(60, ratio, 0.1, 10000);

    glMatrixMode(GL_MODELVIEW);

}

void Display::pressKeys(unsigned char key, int x, int y){

    light.setSpeed(0.1);

    keyStates[key] = true;

}

void Display::releaseKeys(unsigned char key, int x, int y){
    keyStates[key] = false;
}

void Display::mouseMove(int x, int y){

    if(x > (glutGet(GLUT_WINDOW_WIDTH) / 2)){
        camera.lookRight();
    }
    else if( x < (glutGet(GLUT_WINDOW_WIDTH) / 2)){
        camera.lookLeft();
    }
    else{
        camera.stop();
    }
}

void Display::registerCallbacks(){

    glutDisplayFunc(renderScene);
    glutReshapeFunc(reshape);
    glutIdleFunc(renderScene);

    glutKeyboardFunc(pressKeys);
    glutKeyboardUpFunc(releaseKeys);
    glutSpecialFunc(pressSpecialKeys);

    glutPassiveMotionFunc(mouseMove);

}

void Display::pressSpecialKeys(int key, int x, int y){
    switch(key){
        case GLUT_KEY_F1:
            showMenu = !showMenu;
            break;
    }
}

void Display::run(){

    glutCreateWindow(this->winTitle);

    glutFullScreen();

    this->setOpenGLAttributes();

    skybox1.loadObj("./models/skybox1.obj");
    skybox2.loadObj("./models/skybox2.obj");
    skybox3.loadObj("./models/skybox3.obj");
    skybox4.loadObj("./models/skybox4.obj");
    skybox5.loadObj("./models/skybox5.obj");
    skybox6.loadObj("./models/skybox6.obj");

    land1.loadObj("./models/level1.obj");
    land2.loadObj("./models/level1.obj");

    water.loadObj("./models/water.obj");

    box.loadObj("./models/coin.obj");
    box1.loadObj("./models/coin.obj");
    box2.loadObj("./models/coin.obj");
    box3.loadObj("./models/coin.obj");
    box4.loadObj("./models/coin.obj");
    box5.loadObj("./models/coin.obj");
    box6.loadObj("./models/coin.obj");
    box7.loadObj("./models/coin.obj");
    box8.loadObj("./models/coin.obj");
    box9.loadObj("./models/coin.obj");
    box10.loadObj("./models/coin.obj");
    box11.loadObj("./models/coin.obj");
    box12.loadObj("./models/coin.obj");
    box13.loadObj("./models/coin.obj");
    box14.loadObj("./models/coin.obj");
    box15.loadObj("./models/coin.obj");
    box16.loadObj("./models/coin.obj");

    house.loadObj("./models/house.obj");
    house2.loadObj("./models/house2.obj");

    boat.loadObj("./models/boat.obj");

    coin1.loadObj("./models/coin.obj");

    player.init();

    princess.init();

    this->registerCallbacks();

    glutMainLoop();

}
