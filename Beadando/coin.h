#ifndef COIN_H
#define COIN_H
#include "object.h"

class Coin : public Object
{
    public:
        Coin(GLdouble x, GLdouble y, GLdouble z, char* textpath, GLenum texttype, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular, int shininess);

    protected:

    private:
};

#endif // COIN_H
