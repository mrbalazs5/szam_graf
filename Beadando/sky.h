#ifndef SKY_H
#define SKY_H
#include "object.h"

class Sky : public Object
{
    public:
        Sky(GLdouble x, GLdouble y, GLdouble z, char* textpath, GLenum texttype, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular, int shininess);

    protected:

    private:
};

#endif // SKY_H
