#ifndef WATER_H
#define WATER_H
#include "object.h"

class Water : public Object
{
    public:
        Water(GLdouble x, GLdouble y, GLdouble z, char* textpath, GLenum texttype, vector < GLfloat > ambient, vector < GLfloat > diffuse, vector < GLfloat > specular, int shininess);

    protected:

    private:
};

#endif // WATER_H
