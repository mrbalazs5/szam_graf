#ifndef COLLISIONMANAGER_H
#define COLLISIONMANAGER_H
#include <map>
#include "object.h"
#include "player.h"
#include "box.h"
#include "water.h"
#include "land.h"
#include "house.h"
#include "coin.h"
#include "math.h"
#include <typeinfo>
using std::string;
using std::map;

class CollisionManager
{
    public:
        CollisionManager(Water &water, House &house, Player &player, Player &princess);
        void init(map < string, Box > boxes, map < string, Land > lands, map < string, Coin > coins, Water &water, House &house, Player &player, Player &princess);
        void watch();

    protected:

    private:
        map < string, Box > boxes;
        map < string, Land > lands;
        map < string, Coin > coins;
        House &house;
        Water &water;
        Player &player;
        Player &princess;
        GLdouble gravity;

        void checkLands();
        void checkBoxes();
        void checkCoins();
        template <class Object>
        bool checkCollision(Object object, GLdouble x, GLdouble y);
};

#endif // COLLISIONMANAGER_H
